using System;
using MoneySpaceSDK;
using MoneySpaceSDK.Common.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Microsoft.Extensions.Configuration
{
    public static class MoneySpaceServiceCollectionExtensions
    {
        /// <summary>
        /// Registers the default MoneySpace SDK services to the provided <paramref name="serviceCollection"/>.
        /// </summary>
        /// <param name="serviceCollection">The service collection to add to.</param>
        /// <param name="configuration">The MoneySpace configuration.</param>
        /// <returns>The service collection with registered MoneySpace SDK services.</returns>
        public static IServiceCollection AddMoneySpaceSdk(this IServiceCollection serviceCollection, MoneySpaceConfiguration configuration)
        {
            if (serviceCollection == null) throw new ArgumentNullException(nameof(serviceCollection));
            if (configuration == null) throw new ArgumentNullException(nameof(configuration));

            serviceCollection.AddSingleton(configuration);
            serviceCollection.AddSingleton<IMoneySpaceApi, MoneySpaceApi>();

            return serviceCollection;
        }
        
        /// <summary>
        /// Registers the default MoneySpace SDK services to the provided <paramref name="serviceCollection"/>.
        /// </summary>
        /// <param name="serviceCollection">The service collection to add to.</param>
        /// <param name="configuration">The Microsoft configuration used to obtain the MoneySpace SDK configuration.</param>
        /// <returns>The service collection with registered MoneySpace SDK services.</returns>
        public static IServiceCollection AddMoneySpaceSdk(this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            if (serviceCollection == null) throw new ArgumentNullException(nameof(serviceCollection));
            if (configuration == null) throw new ArgumentNullException(nameof(configuration));

            var moneySpaceOptions = configuration.GetMoneySpaceOptions();
            return serviceCollection.AddMoneySpaceSdk(moneySpaceOptions.CreateConfiguration());
        }
    }
}