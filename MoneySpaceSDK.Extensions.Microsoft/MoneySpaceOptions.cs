using MoneySpaceSDK.Common.Configuration;

namespace MoneySpaceSDK.Extensions.Microsoft
{
    /// <summary>
    /// Defines the options needed to configure the MoneySpace.net SDK for .NET that can be initialized using the Microsoft configuration framework.
    /// </summary>
    public class MoneySpaceOptions
    {
        /// <summary>
        /// Gets or sets your MoneySpace Secret Id.
        /// </summary>
        /// <value></value>
        public string SecretId { get; set; }
        
        /// <summary>
        /// Gets or sets your MoneySpace Secret Key.
        /// </summary>
        /// <value></value>
        public string SecretKey { get; set; }
        
        /// <summary>
        /// Gets or sets the API endpoint to connect to.
        /// </summary>
        public string Uri { get; set; }
        
        /// <summary>
        /// Creates a <see cref="MoneySpaceSDK.Common.Configuration.MoneySpaceConfiguration"/> needed to configure the SDK.
        /// </summary>
        /// <returns>The initializes configuration.</returns>
        public MoneySpaceConfiguration CreateConfiguration()
        {
            var moneySpaceConfiguration = string.IsNullOrWhiteSpace(Uri)
                ? new MoneySpaceConfiguration(SecretId, SecretKey)
                : new MoneySpaceConfiguration(SecretKey, SecretKey, Uri);

            return moneySpaceConfiguration;
        }
    }
}