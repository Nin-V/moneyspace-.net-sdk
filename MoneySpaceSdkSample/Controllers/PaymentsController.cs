﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MoneySpaceSDK;
using MoneySpaceSDK.Merchant.Model;
using MoneySpaceSDK.Merchant.Model.Enum;

namespace MoneySpaceSdkSample.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentsController : ControllerBase
    {
        private readonly IMoneySpaceApi _moneySpaceApi;

        public PaymentsController(IMoneySpaceApi moneySpaceApi)
        {
            _moneySpaceApi = moneySpaceApi;
        }

        // POST api/payments
        [HttpPost("")]
        public async Task<IActionResult> CreatePayment()
        {
            // Fake transaction details
            var createPaymentRequest = new CreatePaymentRequest
            {
                FirstName = "example",
                LastName = "example",
                Email = "example@test.com",
                Phone = "0888888888",
                Description = "T-shirt 001 ,pricing 100.25 baht",
                Address = "Address 111/22",
                Message = "After shipping, please share me the delivery receipt",
                Amount = 0.25,
                FeeType = FeeType.Include,
                CustomerOrderId = "ShopDemoOrder_023",
                GatewayType = GatewayType.Card,
                SuccessUrl = "https://www.moneyspace.net/merchantapi/paycardsuccess",
                FailUrl = "https://www.moneyspace.net?status=fail",
                CancelUrl = "https://www.moneyspace.net?status=cancel"
            };

            // Try to create payment
            var createdPayment = await _moneySpaceApi.Merchant.CreateAsync(createPaymentRequest);

            // If something went wrong
            if (!createdPayment.IsSuccess)
            {
                return BadRequest(new { Error = createdPayment.Status });
            }
            
            // Transaction created successfully
            return Ok( new { TransactonId =  createdPayment.TransactionId });
        }
        
        // POST api/payments
        [HttpPost("installment")]
        public async Task<IActionResult> CreateInstallment()
        {
            // Fake transaction details
            var createInstallmentRequest = new CreateInstallmentRequest
            {
                FirstName = "example",
                LastName = "example",
                Email = "example@test.com",
                Phone = "0888888888",
                Description = "T-shirt 001 ,pricing 100.25 baht",
                Address = "Address 111/22",
                Message = "After shipping, please share me the delivery receipt",
                Amount = 3100.01,
                FeeType = FeeType.Include,
                CustomerOrderId = "ShopDemoOrder_020",
                SuccessUrl = "https://www.moneyspace.net/merchantapi/paycardsuccess",
                FailUrl = "https://www.moneyspace.net?status=fail",
                CancelUrl = "https://www.moneyspace.net?status=cancel",
                BankType = BankType.BAY,
                StartTerm = StartTerm.Three,
                EndTerm = EndTerm.Ten
            };

            // Try to create payment
            var createdInstallment = await _moneySpaceApi.Merchant.CreateInstallmentAsync(createInstallmentRequest);

            // If something went wrong
            if (!createdInstallment.IsSuccess)
            {
                return BadRequest(new { Error = createdInstallment.Status });
            }
            
            // Transaction created successfully
            return Ok( new { TransactonId =  createdInstallment.TransactionId });
        }
        
        // GET api/payments/check-transaction
        [HttpGet("check-transaction")]
        public async Task<IActionResult> CheckTransaction([FromQuery] string transactionId)
        {
            var checkTransactionRequest = new CheckTransactionRequest
            {
                TransactionId = transactionId,
            };

            // Check transaction
            var checkTransactionResponse = await _moneySpaceApi.Merchant.CheckTransactionAsync(checkTransactionRequest);

            // If any error while checking
            if (!checkTransactionResponse.IsSuccess)
            {
                return BadRequest(new { Error = checkTransactionResponse.Status });
            }

            // Check was successful
            return Ok( checkTransactionResponse );
        }
        
        // GET api/payments/check-order
        [HttpGet("check-order")]
        public async Task<IActionResult> CheckOrder([FromQuery] string orderId)
        {
            var checkOrderIdRequest = new CheckOrderIdRequest
            {
                OrderId = orderId,
            };

            // Check order
            var checkOrderIdResponse = await _moneySpaceApi.Merchant.CheckOrderIdAsync(checkOrderIdRequest);

            // If any error while checking
            if (!checkOrderIdResponse.IsSuccess)
            {
                return BadRequest(new { Error = checkOrderIdResponse.Status });
            }

            // Check was successful
            return Ok( checkOrderIdResponse );
        }
        
        // GET api/payments/link-payment
        [HttpGet("link-payment")]
        public async Task<IActionResult> LinkPayment([FromQuery] string transactionId)
        {
            // Get payment link (user need to be redirected to this link for purchase)
            // Or View it as a QR Image
            var linkPaymentResponse = await _moneySpaceApi.Merchant.GetPaymentCardLinkAsync(transactionId);

            return Ok( linkPaymentResponse.ToString() );
        }
        
        // GET api/payments/store-info
        [HttpGet("store-info")]
        public async Task<IActionResult> StoreInfo()
        {
            // Get store info
            var storeResponse = await _moneySpaceApi.Merchant.StoreAsync();

            // Usage of store info requests response
            return Ok( storeResponse.Store.First() );
        }
        
        // POST api/payments/webhook-receiver
        [HttpPost("webhook-receiver")]
        public IActionResult WebHookReceiver([FromForm] WebHookResponse webHookResponse)
        {
            // Verify WebHook hash for integrity
            if (!_moneySpaceApi.WebHookHelper.VerifyResponse(webHookResponse))
            {
                // WebHook parameters is not real.
                return BadRequest( new { Error = "Error when verifying the WebHook request." }  );
            }
            
            // WebHook is true and verified.
            // Do what you want.
            switch (webHookResponse.Status)
            {
                case PaymentStatus.Ok:
                    
                    break;
                case PaymentStatus.Pending:
                    
                    break;
                case PaymentStatus.Fail:
                    
                    break;
                case PaymentStatus.Cancel:
                    
                    break;
                case PaymentStatus.PaySuccess:
                    
                    break;
            }

            return Ok();
        }

    }
}