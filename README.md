MoneySpace SDK for .NET and .NET Core
====================

[![NuGet](https://img.shields.io/nuget/v/MoneySpaceSDK.svg)](https://www.nuget.org/packages/MoneySpaceSDK)

The **MoneySpace SDK for .NET and .NET Core** enables .NET developers to easily work with MoneySpace API. 
It supports .NET Framework 4.5+ and .NET Core.

## Quickstart

To get started install the [`MoneySpaceSDK`](https://www.nuget.org/packages/MoneySpaceSDK) package from NuGet. 

Initialize a `MoneySpaceApi` to access the operations for each API:

```
#!c#
    var api = MoneySpaceApi.Create("secretId", "secretKey");
    var createPaymentRequest = new CreatePaymentRequest
    {
        FirstName = "example",
        LastName = "example",
        Email = "example@test.com",
        Phone = "0888888888",
        Description = "T-shirt 001 ,pricing 100.25 baht",
        Address = "Address 111/22",
        Message = "After shipping, please share me the delivery receipt",
        Amount = 0.25, // It's important to have only 2 decimical place
        FeeType = FeeType.Include, // Use FeeType.Exclude if you want shopper to pay fees
        CustomerOrderId = "ShopDemoOrder_013", // Unique
        GatewayType = GatewayType.Card, // use GatewayType.Qrprom if you want qr code payment method, GatewayType.Qrprom can only be used with FeeType.Include
        SuccessUrl = "https://www.moneyspace.net/merchantapi/paycardsuccess",
        FailUrl = "https://www.moneyspace.net?status=fail",
        CancelUrl = "https://www.moneyspace.net?status=cancel"
    };
    var createdPayment = await api.Merchant.CreateAsync(createPaymentRequest);
```

### .NET Core Applications

The [`MoneySpaceSDK.Extensions.Microsoft`](https://www.nuget.org/packages/MoneySpaceSDK.Extensions.Microsoft) package makes it easy to add the MoneySpace SDK to your .NET Core applications.

Once installed register the SDK with the built-in DI container in `Startup.cs`:

```
#!c#
public void ConfigureServices(IServiceCollection services)
{
    // ...
    var conf = new MoneySpaceConfiguration("secretId", "secretKey");
    services.AddMoneySpaceSdk(conf);   
}
```

Then take a dependency on `IMoneySpaceApi` in your class constructor:

```
#!c#
public class PaymentController : ControllerBase
{
    private readonly IMoneySpaceApi _moneySpaceApi;

    public CheckoutController(IMoneySpaceApi moneySpaceApi)
    {
        _moneySpaceApi = moneySpaceApi ?? throw new ArgumentNullException(nameof(moneySpaceApi));
    }

    // etc.

}
```

To configure the SDK using [.NET Core Configuration](https://github.com/aspnet/Configuration) pass your application's `IConfiguration`:

```
#!c#
public class Startup
{
    public Startup(IConfiguration configuration, IHostingEnvironment env)
    {
        Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    public void ConfigureServices(IServiceCollection services)
    {
        // ...
        services.AddMoneySpaceSDK(Configuration);    
    }
}
```

You can then configure `appsettings.json` file with your MoneySpace details:

```
#!json
{
    "MoneySpace": {
        "SecretId": "secretId",
        "SecretKey" : "secretKey"
    }
}
```

## Available Methods

#### Create Payment Request

---
**IMPORTANT**   
It's important to have only **2 decimical place** in Amount   
GatewayType.Qrprom can only be used with **FeeType.Include**   
CustomerOrderId need to be **unique**. You can generate GUID for that field
---

```
#!c#
var createPaymentRequest = new CreatePaymentRequest
{
    FirstName = "example",
    LastName = "example",
    Email = "example@test.com",
    Phone = "0888888888",
    Description = "T-shirt 001 ,pricing 100.25 baht",
    Address = "Address 111/22",
    Message = "After shipping, please share me the delivery receipt",
    Amount = 0.25, // It's important to have only 2 decimical place
    FeeType = FeeType.Include, // Use FeeType.Exclude if you want shopper to pay fees
    CustomerOrderId = "ShopDemoOrder_013",
    GatewayType = GatewayType.Card, // use GatewayType.Qrnone if you want qr code payment method, GatewayType.Qrprom can only be used with FeeType.Include
    SuccessUrl = "https://www.moneyspace.net/merchantapi/paycardsuccess",
    FailUrl = "https://www.moneyspace.net?status=fail",
    CancelUrl = "https://www.moneyspace.net?status=cancel"
};

var createdPayment = await _moneySpaceApi.Merchant.CreateAsync(createPaymentRequest);

// If error
if (!createdPayment.IsSuccess)
{
    return BadRequest(new { Error = createdPayment.Status });
}
```

#### Create Installment Request

---
**IMPORTANT**   
It's important to have only **2 decimical place** in Amount   
CustomerOrderId need to be **unique**. You can generate GUID for that field
---

```
#!c#
var createInstallmentRequest = new CreateInstallmentRequest
{
    FirstName = "example",
    LastName = "example",
    Email = "example@test.com",
    Phone = "0888888888",
    Description = "T-shirt 001 ,pricing 100.25 baht",
    Address = "Address 111/22",
    Message = "After shipping, please share me the delivery receipt",
    Amount = 3100.01, // It's important to have only 2 decimical place
    FeeType = FeeType.Include, // Use FeeType.Exclude if you want shopper to pay fees
    CustomerOrderId = "ShopDemoOrder_013",
    SuccessUrl = "https://www.moneyspace.net/merchantapi/paycardsuccess",
    FailUrl = "https://www.moneyspace.net?status=fail",
    CancelUrl = "https://www.moneyspace.net?status=cancel",
    BankType = BankType.FCY,
    StartTerm = StartTerm.Three,
    EndTerm = EndTerm.Ten
};

var createdInstallment = await _moneySpaceApi.Merchant.CreateAsync(createInstallmentRequest);

// If error
if (!createdInstallment.IsSuccess)
{
    return BadRequest(new { Error = createdInstallment.Status });
}
```

#### Check transaction

```
#!c#
var checkTransactionRequest = new CheckTransactionRequest
{
    TransactionId = "transactionId",
};

var checkTransactionResponse = await _moneySpaceApi.Merchant.CheckTransactionAsync(checkTransactionRequest);

// If error
if (!checkTransactionResponse.IsSuccess)
{
    return BadRequest(new { Error = checkTransactionResponse.Status });
}
```

#### Check OrderId

```
#!c#
var checkOrderIdRequest = new CheckOrderIdRequest
{
    OrderId = "orderId",
};

var checkOrderIdResponse = await _moneySpaceApi.Merchant.CheckOrderIdAsync(checkOrderIdRequest);

// If error
if (!checkOrderIdResponse.IsSuccess)
{
    return BadRequest(new { Error = checkOrderIdResponse.Status });
}
```

#### Get payment link

```
#!c#
var linkPaymentResponse = await _moneySpaceApi.Merchant.GetPaymentCardLinkAsync("transactionId");
```

#### Store info

```
#!c#
var storeResponse = await _moneySpaceApi.Merchant.StoreAsync();
var store = storeResponse.Store.First();
```

#### WebHook Receiver
First you need to define action in controller which will bind webhook response from form data.
Then we will check if response have right hash.

```
#!c#
public IActionResult WebHookReceiver([FromForm] WebHookResponse webHookResponse)
{
    // Verify WebHook hash for integrity
    if (!_moneySpaceApi.WebHookHelper.VerifyResponse(webHookResponse))
    {
        // WebHook parameters is not real.
        return BadRequest( new { Error = "Error when verifying the WebHook request." }  );
    }
    
    // WebHook is true and verified.
    // Do what you want.
    switch (webHookResponse.Status)
    {
        case PaymentStatus.Ok:
            
            break;
        case PaymentStatus.Pending:
            
            break;
        case PaymentStatus.Fail:
            
            break;
        case PaymentStatus.Cancel:
            
            break;
        case PaymentStatus.PaySuccess:
            
            break;
    }

    return Ok();
}
```

## Enums

### GatewayType
Payment method   
Card: `GatewayType.Card`   
Qr Code Payment: `GatewayType.Qrnone`

### FeeType
Who pays fees   
Merchant: `FeeType.Include`   
Shopper: `FeeType.Exclude`

### BankType
Bank Type  
KTC: `BankType.KTC`   
BAY: `BankType.BAY`   
FCY: `BankType.FCY`

### StartTerm
Start term   
3: `StartTerm.Three`   
4: `StartTerm.Four`   
5: `StartTerm.Five`   
6: `StartTerm.Six`   
7: `StartTerm.Seven`   
8: `StartTerm.Eight`   
9: `StartTerm.Nine`   
10: `StartTerm.Ten`

### EndTerm
End term  
3: `EndTerm.Three`   
4: `EndTerm.Four`   
6: `EndTerm.Six`   
9: `EndTerm.Nine`   
10: `EndTerm.Ten`   
12: `EndTerm.Twelve`   
18: `EndTerm.Eighteen`   
24: `EndTerm.TwentyFour`   
36: `EndTerm.ThirtySix`

## Versioning
MoneySpace SDK uses [Semantic Versioning](https://semver.org/). The latest stable code can be found on the `master` branch and will be published to NuGet.