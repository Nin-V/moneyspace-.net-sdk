using MoneySpaceSDK.Helpers.WebHook;
using MoneySpaceSDK.Merchant;

namespace MoneySpaceSDK
{
    /// <summary>
    ///     Convenience interface that provides access to the available MoneySpace.net APIs.
    /// </summary>
    public interface IMoneySpaceApi
    {
        /// <summary>
        ///     Gets the Merchant API.
        /// </summary>
        IMerchantClient Merchant { get; }

        /// <summary>
        ///     Gets the WebHook Helper.
        /// </summary>
        IWebHookHelper WebHookHelper { get; }
    }
}