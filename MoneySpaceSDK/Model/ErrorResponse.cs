using Newtonsoft.Json;

namespace MoneySpaceSDK.Model
{
    public class ErrorResponse
    {
        /// <summary>
        ///     Gets or sets status message in error response
        /// </summary>
        [JsonProperty("status")]
        public string Status { get; set; }
    }
}