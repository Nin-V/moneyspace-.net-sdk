using MoneySpaceSDK.Common.Configuration;
using MoneySpaceSDK.Communication;
using MoneySpaceSDK.Helpers.WebHook;
using MoneySpaceSDK.Merchant;

namespace MoneySpaceSDK
{
    /// <summary>
    ///     Default implementation of <see cref="IMoneySpaceApi" /> that defines the available MoneySpace.net APIs.
    /// </summary>
    public class MoneySpaceApi : IMoneySpaceApi
    {
        /// <summary>
        ///     Creates a new <see cref="MoneySpaceApi" /> instance and initializes each underlying API client.
        /// </summary>
        /// <param name="apiClient">The API client used to send API requests and handle responses.</param>
        /// <param name="configuration">A configuration object containing authentication and API specific information.</param>
        internal MoneySpaceApi(IApiClient apiClient, MoneySpaceConfiguration configuration)
        {
            Merchant = new MerchantClient(apiClient, configuration);
            WebHookHelper = new WebHookHelper(configuration);
        }

        /// <summary>
        ///     Creates a new <see cref="MoneySpaceApi" /> instance and initializes each underlying API client.
        /// </summary>
        /// <param name="configuration">A configuration object containing authentication and API specific information.</param>
        public MoneySpaceApi(MoneySpaceConfiguration configuration)
        {
            var apiClient = new ApiClient(configuration);
            Merchant = new MerchantClient(apiClient, configuration);
            WebHookHelper = new WebHookHelper(configuration);
        }

        /// <inheritdoc />
        public IMerchantClient Merchant { get; }

        /// <inheritdoc />
        public IWebHookHelper WebHookHelper { get; }

        /// <summary>
        ///     Creates a new <see cref="MoneySpaceApi" /> instance with default dependencies.
        /// </summary>
        /// <param name="secretId">Your secret id obtained from the MoneySpace portal.</param>
        /// <param name="secretKey">Your secret key obtained from the MoneySpace portal.</param>
        /// <returns>The configured instance.</returns>
        public static MoneySpaceApi Create(string secretId, string secretKey)
        {
            var configuration = new MoneySpaceConfiguration(secretId, secretKey);

            var apiClient = new ApiClient(configuration);
            return new MoneySpaceApi(apiClient, configuration);
        }

        /// <summary>
        ///     Creates a new <see cref="MoneySpaceApi" /> instance with default dependencies.
        /// </summary>
        /// <param name="secretId">Your secret id obtained from the MoneySpace portal.</param>
        /// <param name="secretKey">Your secret key obtained from the MoneySpace portal.</param>
        /// <param name="uri">The base URL of the MoneySpace API you wish to connect to.</param>
        /// <returns>The configured instance.</returns>
        public static MoneySpaceApi Create(string secretId, string secretKey, string uri)
        {
            var configuration = new MoneySpaceConfiguration(secretId, secretKey, uri);

            var apiClient = new ApiClient(configuration);
            return new MoneySpaceApi(apiClient, configuration);
        }
    }
}