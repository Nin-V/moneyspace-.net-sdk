using System;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace MoneySpaceSDK.Attributes
{
    /// <summary>
    ///     Specifies the type of string data allowed in a property.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter,
        AllowMultiple = false)]
    public class OnlyUppercaseAndNumbersAttribute : ValidationAttribute
    {

        private new const string ErrorMessage = "The field {0} can only contain uppercase letters and numbers";
        /// <summary>
        ///     Default constructor.
        /// </summary>
        /// <remarks>
        ///     This constructor selects a reasonable default error message for
        ///     <see cref="ValidationAttribute.FormatErrorMessage" />
        /// </remarks>
        public OnlyUppercaseAndNumbersAttribute()
            : base(ErrorMessage)
        {
        }
        
        public override bool IsValid(object value)
        {
            string str;
            // Automatically pass if value is null. RequiredAttribute should be used to assert a value is not null.
            if (value == null)
            {
                return true;
            }
            if (value is string val)
            {
                str = val;
            }
            else
            {
                throw new InvalidCastException("Field is not string");
            }
            
            Regex rg = new Regex(@"^[A-Z0-9]*$");

            return rg.IsMatch(str);
        }
        
        /// <summary>
        ///     Applies formatting to a specified error message. (Overrides <see cref="ValidationAttribute.FormatErrorMessage" />)
        /// </summary>
        /// <param name="name">The name to include in the formatted string.</param>
        /// <returns>A localized string to describe the maximum acceptable length.</returns>
        public override string FormatErrorMessage(string name) =>
            string.Format(ErrorMessageString, name);
    }
}