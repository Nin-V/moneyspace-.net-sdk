using System;

namespace MoneySpaceSDK.Exceptions
{
    /// <summary>
    ///     Base class for exceptions thrown by the MoneySpace.net SDK.
    /// </summary>
    public class MoneySpaceException : Exception
    {
        /// <summary>
        ///     Creates a new <see cref="MoneySpaceException" /> instance with the provided message.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        /// <returns></returns>
        public MoneySpaceException(string message) : base(message)
        {
        }
    }
}