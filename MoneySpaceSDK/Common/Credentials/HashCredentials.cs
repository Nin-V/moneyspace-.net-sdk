using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using MoneySpaceSDK.Common.Calculators;
using MoneySpaceSDK.Common.Configuration;

namespace MoneySpaceSDK.Common.Credentials
{
    /// <summary>
    ///     Credentials class that authorizes HTTP requests using your MoneySpace.net.
    /// </summary>
    internal class HashCredentials : IApiCredentials
    {
        private readonly MoneySpaceConfiguration _configuration;
        private readonly IHashCalculator _hashCalculator;
        private readonly IEnumerable<string> _hashOrder;

        /// <summary>
        ///     Creates a new see <cref name="HashCredentials" /> instance.
        /// </summary>
        /// <param name="configuration">The MoneySpace configuration containing your secret key.</param>
        /// <param name="hashOrder">The hash calculator.</param>
        /// <param name="hashCalculator">The hash calculator.</param>
        public HashCredentials(MoneySpaceConfiguration configuration, IEnumerable<string> hashOrder,
            IHashCalculator hashCalculator = null)
        {
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
            _hashOrder = hashOrder;
            _hashCalculator = hashCalculator ?? new HmacCalculator();
        }

        /// <summary>
        ///     Authorizes the request.
        /// </summary>
        /// <param name="httpRequest">The HTTP request to authorize.</param>
        /// <param name="input">The dictionary that contains post data.</param>
        /// <returns>A task that completes when the provided <paramref name="httpRequest" /> has been authorized.</returns>
        public Task AuthorizeAsync(HttpRequestMessage httpRequest, IDictionary<string, string> input = null)
        {
            if (httpRequest == null)
                throw new ArgumentNullException(nameof(httpRequest));

            if (string.IsNullOrEmpty(_configuration.SecretId))
                throw new ArgumentException("Your Secret Id must be configured", nameof(_configuration.SecretId));

            if (string.IsNullOrEmpty(_configuration.SecretKey))
                throw new ArgumentException("Your Secret Key must be configured", nameof(_configuration.SecretKey));

            var qs = HttpUtility.ParseQueryString(httpRequest.RequestUri.Query);
            qs.Set("secreteID", _configuration.SecretId);

            var secret = new StringBuilder();

            if (input != null)
            {
                input.Add("secreteID", _configuration.SecretId);
                foreach (var order in _hashOrder)
                    if (input.ContainsKey(order))
                        secret.Append(input[order]);
            }
            else
            {
                foreach (var order in _hashOrder)
                    if (qs[order] != null)
                        secret.Append(qs[order]);
            }

            var hash = _hashCalculator.ComputeHash(secret.ToString(), _configuration.SecretKey);

            qs.Set("hash", hash);

            var uriBuilder = new UriBuilder(httpRequest.RequestUri) {Query = qs.ToString()};

            var newUri = uriBuilder.Uri;

            httpRequest.RequestUri = newUri;

            return Task.FromResult(0);
        }
    }
}