using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace MoneySpaceSDK.Common.Credentials
{
    /// <summary>
    ///     Defines an interface for different authorization/authentication schemes/types.
    /// </summary>
    internal interface IApiCredentials
    {
        /// <summary>
        ///     Authorizes the request.
        /// </summary>
        /// <param name="httpRequest">The HTTP request to authorize.</param>
        /// <param name="input">The dictionary that contains post data.</param>
        /// <returns>A task that completes when the provided <paramref name="httpRequest" /> has been authorized.</returns>
        Task AuthorizeAsync(HttpRequestMessage httpRequest, IDictionary<string, string> input);
    }
}