namespace MoneySpaceSDK.Common.Calculators
{
    internal interface IHashCalculator
    {
        string ComputeHash(string input, string key);
    }
}