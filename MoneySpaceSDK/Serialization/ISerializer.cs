using System.Collections.Generic;

namespace MoneySpaceSDK.Serialization
{
    /// <summary>
    ///     Defines an interface for classes that provide serialization to and from string formats such as JSON.
    /// </summary>
    public interface ISerializer
    {
        /// <summary>
        ///     Serializes the provided <paramref name="input" /> to string.
        /// </summary>
        /// <param name="input">The object to serialize.</param>
        /// <returns>The input serialized as string.</returns>
        string Serialize(object input);

        /// <summary>
        ///     Deserializes the provided <paramref name="input" /> to the specified <typeparamref name="T" />.
        /// </summary>
        /// <typeparam name="T">Object Type to deserialize into</typeparam>
        /// <param name="input">The input to deserialize.</param>
        /// <returns>The deserialized object.</returns>
        T Deserialize<T>(string input);

        /// <summary>
        ///     Serializes the provided <paramref name="input" /> to key value for form data.
        /// </summary>
        /// <param name="input">The input to serialize.</param>
        /// <returns>The dictionary.</returns>
        IDictionary<string, string> ToKeyValue(object input);
    }
}