using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace MoneySpaceSDK.Serialization
{
    internal class JsonSerializer : ISerializer
    {
        private readonly Newtonsoft.Json.JsonSerializer _jsonSerializer;
        private readonly JsonSerializerSettings _serializerSettings;

        /// <summary>
        ///     Creates a new <see cref="JsonSerializer" /> that allows customization of the underlying
        ///     JSON.NET serializer settings.
        /// </summary>
        /// <param name="configureSettings">An action to be run against the JSON.NET serializer settings.</param>
        public JsonSerializer(Action<JsonSerializerSettings> configureSettings = null)
        {
            _serializerSettings = CreateSerializerSettings(configureSettings);
            _jsonSerializer = Newtonsoft.Json.JsonSerializer.Create(_serializerSettings);
        }

        /// <inheritdoc />
        public string Serialize(object input)
        {
            if (input == null) throw new ArgumentNullException(nameof(input));
            return JsonConvert.SerializeObject(input, _serializerSettings);
        }

        /// <inheritdoc />
        public T Deserialize<T>(string input)
        {
            return JsonConvert.DeserializeObject<T>(input, _serializerSettings);
        }

        /// <inheritdoc />
        public IDictionary<string, string> ToKeyValue(object input)
        {
            if (input == null) return null;

            var token = input as JToken;
            if (token == null) return ToKeyValue(JObject.FromObject(input, _jsonSerializer));

            if (token.HasValues)
            {
                var contentData = new Dictionary<string, string>();
                foreach (var child in token.Children().ToList())
                {
                    var childContent = ToKeyValue(child);
                    if (childContent != null)
                        contentData = contentData.Concat(childContent)
                            .ToDictionary(k => k.Key, v => v.Value);
                }

                return contentData;
            }

            var jValue = token as JValue;
            if (jValue?.Value == null) return null;

            var value = jValue.Type == JTokenType.Date
                ? jValue.ToString("o", CultureInfo.InvariantCulture)
                : jValue.ToString(CultureInfo.InvariantCulture);

            return new Dictionary<string, string> {{token.Path, value}};
        }

        private static JsonSerializerSettings CreateSerializerSettings(Action<JsonSerializerSettings> configureSettings)
        {
            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                ContractResolver = new DefaultContractResolver
                {
                    NamingStrategy = new SnakeCaseNamingStrategy()
                },
                Converters = new List<JsonConverter>
                {
                    new IsoDateTimeConverter
                    {
                        DateTimeFormat = "yyyyMMddHHmmss"
                    },
                    new StringEnumConverter
                    {
                        NamingStrategy = new CamelCaseNamingStrategy()
                    }
                }
            };

            configureSettings?.Invoke(settings);

            return settings;
        }
    }
}