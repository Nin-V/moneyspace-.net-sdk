using System;
using System.Threading;
using System.Threading.Tasks;
using MoneySpaceSDK.Merchant.Model;

namespace MoneySpaceSDK.Merchant
{
    public interface IMerchantClient
    {
        /// <summary>
        ///     Request a payment.
        /// </summary>
        /// <param name="paymentRequest">The payment details such as amount and currency.</param>
        /// <param name="cancellationToken">A cancellation token that can be used to cancel the underlying HTTP request.</param>
        /// <returns>A task that upon completion contains the payment response.</returns>
        /// <exception cref="MoneySpaceSDK.Exceptions.InvalidModelException">Throws when payment request is not valid</exception>
        Task<CreatePaymentResponse> CreateAsync(CreatePaymentRequest paymentRequest,
            CancellationToken cancellationToken = default(CancellationToken));
        
        /// <summary>
        ///     Request a installment.
        /// </summary>
        /// <param name="installmentRequest">The payment details such as amount and currency.</param>
        /// <param name="cancellationToken">A cancellation token that can be used to cancel the underlying HTTP request.</param>
        /// <returns>A task that upon completion contains the payment response.</returns>
        /// <exception cref="MoneySpaceSDK.Exceptions.InvalidModelException">Throws when payment request is not valid</exception>
        Task<CreateInstallmentResponse> CreateInstallmentAsync(CreateInstallmentRequest installmentRequest,
            CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        ///     Request a check for transaction.
        /// </summary>
        /// <param name="checkTransactionRequest">The transaction details such as transaction id.</param>
        /// <param name="cancellationToken">A cancellation token that can be used to cancel the underlying HTTP request.</param>
        /// <returns>A task that upon completion contains the payment response.</returns>
        Task<CheckTransactionResponse> CheckTransactionAsync(CheckTransactionRequest checkTransactionRequest,
            CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        ///     Request a check for order.
        /// </summary>
        /// <param name="checkOrderIdRequest">The order details such as order id.</param>
        /// <param name="cancellationToken">A cancellation token that can be used to cancel the underlying HTTP request.</param>
        /// <returns>A task that upon completion contains the payment response.</returns>
        Task<CheckOrderIdResponse> CheckOrderIdAsync(CheckOrderIdRequest checkOrderIdRequest,
            CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        ///     Get link payment
        ///     This feature works on either Credit / debit card or Thai QR code payment
        /// </summary>
        /// <param name="transactionId">Transaction ID that Money Space assigns to each transaction.</param>
        /// <param name="cancellationToken">A cancellation token that can be used to cancel the underlying HTTP request.</param>
        /// <returns>A task that upon completion contains the payment response.</returns>
        Task<Uri> GetPaymentCardLinkAsync(string transactionId,
            CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        ///     Getting Merchant’s Profile
        /// </summary>
        /// <param name="cancellationToken">A cancellation token that can be used to cancel the underlying HTTP request.</param>
        /// <returns>A task that upon completion contains the payment response.</returns>
        Task<HimarResponse> StoreAsync(CancellationToken cancellationToken = default(CancellationToken));
    }
}