using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace MoneySpaceSDK.Merchant.Model
{
    public class CheckOrderIdResponse
    {
        [JsonProperty("Amount ")]
        public string Amount { get; set; }

        [JsonProperty("Order ID ")]
        public string OrderId { get; set; }

        [JsonProperty("Status Payment ")]
        public string StatusPayment { get; set; }

        [JsonProperty("Description  ")]
        public string Description { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }
        
        [NotMapped]
        public bool IsSuccess => !string.IsNullOrWhiteSpace(OrderId);
    }
}