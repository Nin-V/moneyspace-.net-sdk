using Newtonsoft.Json;

namespace MoneySpaceSDK.Merchant.Model
{
    public class HimarResponse
    {
        [JsonProperty("Store")] public StoreResponse[] Store { get; set; }
    }
}