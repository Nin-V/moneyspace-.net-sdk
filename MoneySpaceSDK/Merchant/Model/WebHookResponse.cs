using MoneySpaceSDK.Merchant.Model.Enum;

namespace MoneySpaceSDK.Merchant.Model
{
    public class WebHookResponse
    {
        public string TransactionId { get; set; }

        /// <summary>
        ///     In term of WebHook, parameter of TransactionId (After a payment is done) will be TransectionID
        /// </summary>
        public string TransectionId { get; set; }

        public string Amount { get; set; }

        public PaymentStatus Status { get; set; }

        public string OrderId { get; set; }

        public string Hash { get; set; }
    }
}