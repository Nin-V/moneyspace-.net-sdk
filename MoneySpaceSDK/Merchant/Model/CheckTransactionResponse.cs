using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace MoneySpaceSDK.Merchant.Model
{
    public class CheckTransactionResponse
    {
        [JsonProperty("Amount ")]
        public string Amount { get; set; }

        [JsonProperty("Transaction ID ")]
        public string TransactionId { get; set; }

        [JsonProperty("Status Payment ")]
        public string StatusPayment { get; set; }

        [JsonProperty("Description  ")]
        public string Description { get; set; }

        /// <summary>
        ///     MoneySpace returns 200 OK and status when something went wrong.
        /// </summary>
        [JsonProperty("status")]
        public string Status { get; set; }
        
        [NotMapped]
        public bool IsSuccess => !string.IsNullOrWhiteSpace(TransactionId);
    }
}