using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace MoneySpaceSDK.Merchant.Model.Enum
{
    [JsonConverter(typeof(StringEnumConverter))]
    [DataContract]
    public enum EndTerm
    {
        [EnumMember(Value = "3")]
        Three = 3,
        
        [EnumMember(Value = "4")]
        Four = 4,
        
        [EnumMember(Value = "6")]
        Six = 6,
        
        [EnumMember(Value = "9")]
        Nine = 9,
        
        [EnumMember(Value = "10")]
        Ten = 10,
        
        [EnumMember(Value = "12")]
        Twelve = 12,
        
        [EnumMember(Value = "18")]
        Eighteen = 18,
        
        [EnumMember(Value = "24")]
        TwentyFour = 24,
        
        [EnumMember(Value = "36")]
        ThirtySix = 36
    }
}