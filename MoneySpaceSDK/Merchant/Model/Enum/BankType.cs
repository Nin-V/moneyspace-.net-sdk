using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace MoneySpaceSDK.Merchant.Model.Enum
{
    [JsonConverter(typeof(StringEnumConverter))]
    [DataContract]
    public enum BankType
    {
        [EnumMember(Value = "KTC")]
        KTC,
        
        [EnumMember(Value = "BAY")]
        BAY,
        
        [EnumMember(Value = "FCY")]
        FCY
    }
}