namespace MoneySpaceSDK.Merchant.Model.Enum
{
    /// <summary>
    ///     Status of payment
    /// </summary>
    public enum PaymentStatus
    {
        ///<summary>Creating a successful payment method</summary>
        Ok,

        ///<summary>Waiting for payment</summary>
        Pending,

        ///<summary>Payment success</summary>
        PaySuccess,

        ///<summary>Payment failed</summary>
        Fail,

        ///<summary>Payment canceled</summary>
        Cancel
    }
}