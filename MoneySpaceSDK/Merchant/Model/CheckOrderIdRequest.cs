using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace MoneySpaceSDK.Merchant.Model
{
    public class CheckOrderIdRequest
    {
        [JsonProperty("orderID")]
        [Required]
        public string OrderId { get; set; }

        [JsonProperty("timeHash")]
        public DateTime TimeHash { get; set; }
    }
}