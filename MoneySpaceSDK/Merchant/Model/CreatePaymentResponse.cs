using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace MoneySpaceSDK.Merchant.Model
{
    public class CreatePaymentResponse
    {
        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("Transaction ID")]
        public string TransactionId { get; set; }
        
        [NotMapped]
        public bool IsSuccess => !string.IsNullOrWhiteSpace(TransactionId);
    }
}