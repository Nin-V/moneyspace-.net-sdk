using Newtonsoft.Json;

namespace MoneySpaceSDK.Merchant.Model
{
    public class StoreResponse
    {
        [JsonProperty("name")] public string Name { get; set; }

        [JsonProperty("logo")] public string Logo { get; set; }

        [JsonProperty("telephone")] public string Telephone { get; set; }
    }
}