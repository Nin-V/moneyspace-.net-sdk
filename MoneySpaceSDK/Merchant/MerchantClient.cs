using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MoneySpaceSDK.Common.Configuration;
using MoneySpaceSDK.Common.Credentials;
using MoneySpaceSDK.Communication;
using MoneySpaceSDK.Exceptions;
using MoneySpaceSDK.Merchant.Model;

namespace MoneySpaceSDK.Merchant
{
    /// <summary>
    ///     Default implementation of <see cref="IMerchantClient" />.
    /// </summary>
    internal class MerchantClient : IMerchantClient
    {
        private readonly IApiClient _apiClient;
        private readonly MoneySpaceConfiguration _configuration;

        public MerchantClient(IApiClient apiClient, MoneySpaceConfiguration configuration)
        {
            _apiClient = apiClient ?? throw new ArgumentNullException(nameof(apiClient));
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
        }

        public Task<CreatePaymentResponse> CreateAsync(CreatePaymentRequest paymentRequest,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var context = new ValidationContext(paymentRequest);
            var results = new List<ValidationResult>();
            if (!Validator.TryValidateObject(paymentRequest, context, results))
            {
                throw new InvalidModelException(String.Join("; ", results.Select(x => x.ErrorMessage)));
            }

            const string path = "merchantapi/v2/createpayment/obj";

            var hashOrder = new List<string>
            {
                "firstname", "lastname", "email", "phone", "amount", "currency", "description", "address",
                "message", "feeType", "timeHash", "customer_order_id", "gatewayType", "successUrl", "failUrl",
                "cancelUrl"
            };
            var apiCredentials = new HashCredentials(_configuration, hashOrder);
            paymentRequest.TimeHash = DateTime.UtcNow;
            return _apiClient.PostAsync<CreatePaymentRequest, CreatePaymentResponse>(path, apiCredentials,
                cancellationToken, paymentRequest);
        }

        public Task<CreateInstallmentResponse> CreateInstallmentAsync(CreateInstallmentRequest installmentRequest,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var context = new ValidationContext(installmentRequest);
            var results = new List<ValidationResult>();
            if (!Validator.TryValidateObject(installmentRequest, context, results))
            {
                throw new InvalidModelException(String.Join("; ", results.Select(x => x.ErrorMessage)));
            }

            const string path = "merchantapi/v2/createinstallment/obj";

            var hashOrder = new List<string>
            {
                "firstname", "lastname", "email", "phone", "amount", "currency", "description", "address",
                "message", "feeType", "timeHash", "customer_order_id", "gatewayType", "successUrl", "failUrl",
                "cancelUrl"
            };
            var apiCredentials = new HashCredentials(_configuration, hashOrder);
            installmentRequest.TimeHash = DateTime.UtcNow;
            return _apiClient.PostAsync<CreateInstallmentRequest, CreateInstallmentResponse>(path, apiCredentials,
                cancellationToken, installmentRequest);
        }
        
        

        public Task<CheckTransactionResponse> CheckTransactionAsync(CheckTransactionRequest checkTransactionRequest,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            const string path = "merchantapi/v1/findbytransaction/obj";

            var hashOrder = new List<string>
            {
                "transactionID", "timeHash"
            };
            var apiCredentials = new HashCredentials(_configuration, hashOrder);
            checkTransactionRequest.TimeHash = DateTime.UtcNow;
            return _apiClient.PostAsync<CheckTransactionRequest, CheckTransactionResponse>(path, apiCredentials,
                cancellationToken, checkTransactionRequest);
        }

        public Task<CheckOrderIdResponse> CheckOrderIdAsync(CheckOrderIdRequest checkOrderIdRequest,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            const string path = "merchantapi/v1/findbyorderid/obj";

            var hashOrder = new List<string>
            {
                "orderID", "timeHash"
            };
            var apiCredentials = new HashCredentials(_configuration, hashOrder);
            checkOrderIdRequest.TimeHash = DateTime.UtcNow;
            return _apiClient.PostAsync<CheckOrderIdRequest, CheckOrderIdResponse>(path, apiCredentials,
                cancellationToken, checkOrderIdRequest);
        }

        public Task<Uri> GetPaymentCardLinkAsync(string transactionId,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            const string path = "merchantapi/makepayment/linkpaymentcard";

            var hashOrder = new List<string>
            {
                "transactionID", "timehash"
            };
            var apiCredentials = new HashCredentials(_configuration, hashOrder);
            var qs = "?transactionID=" + transactionId + "&timehash=" + DateTime.UtcNow.ToString("yyyyMMddHHmmss");

            return _apiClient.GetUrl(path + qs, apiCredentials, cancellationToken);
        }

        // Don't ask why.
        public Task<HimarResponse> StoreAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            const string path = "merchantapi/v1/store/obj";
            var hashOrder = new List<string>
            {
                "timeHash", "secreteID"
            };
            var apiCredentials = new HashCredentials(_configuration, hashOrder);
            var qs = "?timeHash=" + DateTime.UtcNow.ToString("yyyyMMddHHmmss");

            return _apiClient.GetAsync<HimarResponse>(path + qs, apiCredentials, cancellationToken);
        }
    }
}