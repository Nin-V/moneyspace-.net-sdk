using System;
using MoneySpaceSDK.Common.Calculators;
using MoneySpaceSDK.Common.Configuration;
using MoneySpaceSDK.Merchant.Model;
using MoneySpaceSDK.Merchant.Model.Enum;

namespace MoneySpaceSDK.Helpers.WebHook
{
    internal class WebHookHelper : IWebHookHelper
    {
        private readonly MoneySpaceConfiguration _configuration;
        private readonly IHashCalculator _hashCalculator;

        public WebHookHelper(MoneySpaceConfiguration configuration, IHashCalculator hashCalculator = null)
        {
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
            _hashCalculator = hashCalculator ?? new HmacCalculator();
        }

        public bool VerifyResponse(WebHookResponse response)
        {
            string input;

            if (!string.IsNullOrWhiteSpace(response.TransactionId))
            {
                input = response.TransactionId + response.Amount;
            }
            else
            {
                var status = response.Status == PaymentStatus.Ok ? "OK" : response.Status.ToString().ToLower();
                input = response.TransectionId + response.Amount + status + response.OrderId;
            }

            var hash = _hashCalculator.ComputeHash(input, _configuration.SecretKey);

            return hash == response.Hash.ToLower();
        }
    }
}